<?php
/**
 * @file
 * Constant definitions for AdminRSS
 *
 * @since DRUPAL-5
 * @license General Public License version 2.0 and later
 */

// Cache id for feed information.
const ADMINRSS_CID           = 'adminrss:feed-info';

// Configuration variables.
const ADMINRSS_VAR_KEY       = 'adminrss_key';

// Menu paths.
const ADMINRSS_PATH_HOME     = 'adminrss';
const ADMINRSS_PATH_SETTINGS = 'admin/config/adminrss';
